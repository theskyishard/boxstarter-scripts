# TODO Add attribution

# Boxstarter options
$Boxstarter.Log="$env:temp\boxstarter.log"
$Boxstarter.RebootOk=$true # Allow reboots?
$Boxstarter.NoPassword=$false # Is this a machine with no login password?
$Boxstarter.AutoLogin=$true # Save my password securely and auto-login after a reboot

# Store script progress for use throughout the script
$CurrentStep = 0

# Add a reg key to cheat a little and store our progress, making this script safely repeatable is beyond my powershell abilities currently
Push-Location
Set-Location HKCU:

$isRegistryKeyPresent = Test-Path .\Software\sysprepvagrantserver2012

if ($isRegistryKeyPresent -eq $False)
{
    New-Item -Path .\Software -Name sysprepvagrantserver2012
    New-ItemProperty ".\Software\sysprepvagrantserver2012" -Name "currentStep" -Value $CurrentStep -PropertyType "DWord"
}

$CurrentStep++
$value = (Get-ItemProperty -Path ".\Software\sysprepvagrantserver2012" -Name currentStep).currentStep

Pop-Location

if ($CurrentStep -gt $value)
{
    # Disable password complexity requirements
    Write-Host "Disabling password complexity requirements..."

    # Attribution header (do not remove)
    #
    # Script block taken from Stack Overflow @ 10:26 12/12/2014
    # Modified by Mitchell Lane
    # Question: Modify Local Security Policy using Powershell (http://stackoverflow.com/questions/23260656/modify-local-security-policy-using-powershell)
    # Contributing authors: Kiquenet (http://stackoverflow.com/users/206730/kiquenet)
    #                       Raf (http://stackoverflow.com/users/3153152/raf)
    #                       Kayasax (http://stackoverflow.com/users/381149/kayasax)
    secedit /export /cfg c:\secpol.cfg
    (gc C:\secpol.cfg).replace("PasswordComplexity = 1", "PasswordComplexity = 0") | Out-File C:\secpol.cfg
    secedit /configure /db c:\windows\security\local.sdb /cfg c:\secpol.cfg /areas SECURITYPOLICY
    rm -force c:\secpol.cfg -confirm:$false

    # Configure Administrator user
    Write-Host "Configuring Administrator user..."

    $admin=[adsi]"WinNT://./Administrator,user"
    $admin.psbase.rename("vagrant")
    $admin.SetPassword("vagrant")
    $admin.UserFlags.value = $admin.UserFlags.value -bor 0x10000
    $admin.CommitChanges()

    Push-Location
    Set-Location HKCU:

    Set-ItemProperty -Path .\Software\sysprepvagrantserver2012 -Name currentStep -Value $CurrentStep

    Pop-Location

    Invoke-Reboot
}

$CurrentStep++

if ($CurrentStep -gt $value)
{
    # Additional disk cleanup - configuration step
    Write-Host "Configuring disk cleanup..."
    Add-WindowsFeature -Name Desktop-Experience

    Push-Location
    Set-Location HKCU:

    Set-ItemProperty -Path .\Software\sysprepvagrantserver2012 -Name currentStep -Value $CurrentStep

    Pop-Location

    if (Test-PendingReboot) { Invoke-Reboot }
}

$CurrentStep++

if ($CurrentStep -gt $value)
{
    Write-Host "Please set the cleanup options to use later in the installation process."
    C:\Windows\System32\cleanmgr.exe /d c: /sageset:1

    # Inform the user that this script is now in unattended mode
    Write-Host "Configuration complete - script can now be left unattended."

    Push-Location
    Set-Location HKCU:

    Set-ItemProperty -Path .\Software\sysprepvagrantserver2012 -Name currentStep -Value $CurrentStep

    Pop-Location
}

$CurrentStep++

if ($CurrentStep -gt $value)
{
    # Configure WinRM
    Write-Host "Configuring WinRM..."

    winrm set winrm/config/client/auth '@{Basic="true"}'
    winrm set winrm/config/service/auth '@{Basic="true"}'
    winrm set winrm/config/service '@{AllowUnencrypted="true"}'

    Push-Location
    Set-Location HKCU:

    Set-ItemProperty -Path .\Software\sysprepvagrantserver2012 -Name currentStep -Value $CurrentStep

    Pop-Location
}

$CurrentStep++

if ($CurrentStep -gt $value)
{
    # Allow Remote Desktop connections
    Write-Host "Configuring remote desktop options..."

    $obj = Get-WmiObject -Class "Win32_TerminalServiceSetting" -Namespace root\cimv2\terminalservices
    $obj.SetAllowTsConnections(1,1)

    Push-Location
    Set-Location HKCU:

    Set-ItemProperty -Path .\Software\sysprepvagrantserver2012 -Name currentStep -Value $CurrentStep

    Pop-Location
}

$CurrentStep++

if ($CurrentStep -gt $value)
{
    # Enable CredSSP (2nd hop) authentication
    Write-Host "Configuring CredSSP..."

    Enable-WSManCredSSP -Force -Role Server

    Push-Location
    Set-Location HKCU:

    Set-ItemProperty -Path .\Software\sysprepvagrantserver2012 -Name currentStep -Value $CurrentStep

    Pop-Location
}

$CurrentStep++

if ($CurrentStep -gt $value)
{
    # Enable firewall rules for RDP and winrm outside of subnet
    Write-Host "Configuring firewall rules..."

    Set-NetFirewallRule -Name WINRM-HTTP-In-TCP-PUBLIC -RemoteAddress Any
    Set-NetFirewallRule -Name RemoteDesktop-UserMode-In-TCP -Enabled True

    Push-Location
    Set-Location HKCU:

    Set-ItemProperty -Path .\Software\sysprepvagrantserver2012 -Name currentStep -Value $CurrentStep

    Pop-Location
}

$CurrentStep++

if ($CurrentStep -gt $value)
{
    # Shrink PageFile
    Write-Host "Configuring pagefile..."

    $System = GWMI Win32_ComputerSystem -EnableAllPrivileges
    $System.AutomaticManagedPagefile = $False
    $System.Put()

    $CurrentPageFile = gwmi -query "select * from Win32_PageFileSetting where name='c:\\pagefile.sys'"
    $CurrentPageFile.InitialSize = 512
    $CurrentPageFile.MaximumSize = 512
    $CurrentPageFile.Put()

    Push-Location
    Set-Location HKCU:

    Set-ItemProperty -Path .\Software\sysprepvagrantserver2012 -Name currentStep -Value $CurrentStep

    Pop-Location
}

$CurrentStep++

if ($CurrentStep -gt $value)
{
    # Powershell execution policy
    Write-Host "Configuring powershell execution policy..."

    Update-ExecutionPolicy RemoteSigned

    Push-Location
    Set-Location HKCU:

    Set-ItemProperty -Path .\Software\sysprepvagrantserver2012 -Name currentStep -Value $CurrentStep

    Pop-Location
}

$CurrentStep++

if ($CurrentStep -gt $value)
{
    # Update Windows and reboot if necessary
    Write-Host "Installing updates..."

    Enable-MicrosoftUpdate
    Install-WindowsUpdate -AcceptEula

    Push-Location
    Set-Location HKCU:

    Set-ItemProperty -Path .\Software\sysprepvagrantserver2012 -Name currentStep -Value $CurrentStep

    Pop-Location

    if (Test-PendingReboot) { Invoke-Reboot }
}

$CurrentStep++

if ($CurrentStep -gt $value)
{
    # Remove windows annoyances - http://www.hurryupandwait.io/blog/deannoyafying-a-default-windows-server-install
    cinstm win-no-annoy

    Push-Location
    Set-Location HKCU:

    Set-ItemProperty -Path .\Software\sysprepvagrantserver2012 -Name currentStep -Value $CurrentStep

    Pop-Location
}

$CurrentStep++

if ($CurrentStep -gt $value)
{
    # Cleanup WinSXS update debris
    Write-Host "Cleaning update debris..."

    Dism.exe /online /Cleanup-Image /StartComponentCleanup /ResetBase

    Push-Location
    Set-Location HKCU:

    Set-ItemProperty -Path .\Software\sysprepvagrantserver2012 -Name currentStep -Value $CurrentStep

    Pop-Location
}

$CurrentStep++

if ($CurrentStep -gt $value)
{
    # Additional disk cleanup - execution step
    Write-Host "Cleaning disk..."

    C:\Windows\System32\cleanmgr.exe /d c: /sagerun:1

    Push-Location
    Set-Location HKCU:

    Set-ItemProperty -Path .\Software\sysprepvagrantserver2012 -Name currentStep -Value $CurrentStep

    Pop-Location
}

$CurrentStep++

if ($CurrentStep -gt $value)
{
    # Uninstall unused windows features
    Write-Host "Configuring Windows features..."

    @('Desktop-Experience', 'InkAndHandwritingServices', 'Server-Media-Foundation', 'Powershell-ISE') | Remove-WindowsFeature
    Get-WindowsFeature | ? { $_.InstallState -eq 'Available' } | Uninstall-WindowsFeature -Remove

    Push-Location
    Set-Location HKCU:

    Set-ItemProperty -Path .\Software\sysprepvagrantserver2012 -Name currentStep -Value $CurrentStep

    Pop-Location

    if (Test-PendingReboot) { Invoke-Reboot }
}

$CurrentStep++

if ($CurrentStep -gt $value)
{
    # Defragment the drive
    Write-Host "Defragmenting drive..."

    Optimize-Volume -DriveLetter C

    Push-Location
    Set-Location HKCU:

    Set-ItemProperty -Path .\Software\sysprepvagrantserver2012 -Name currentStep -Value $CurrentStep

    Pop-Location
}

$CurrentStep++

if ($CurrentStep -gt $value)
{
    # Purge any hidden data
    cinstm wget
    
    wget http://download.sysinternals.com/files/SDelete.zip -OutFile sdelete.zip
    
    Add-Type -Assembly System.IO.Compression.FileSystem
    [System.IO.Compression.ZipFile]::ExtractToDirectory("sdelete.zip", ".") 
    ./sdelete.exe -z c:

    cuninst wget

    Push-Location
    Set-Location HKCU:

    Set-ItemProperty -Path .\Software\sysprepvagrantserver2012 -Name currentStep -Value $CurrentStep

    Pop-Location
}

$CurrentStep++

if ($CurrentStep -gt $value)
{
    # Shutdown - Script will run on next startup, but only to clean up after itself.
    Write-Host "Shutting down machine..."
    
    Push-Location
    Set-Location HKCU:

    Set-ItemProperty -Path .\Software\sysprepvagrantserver2012 -Name currentStep -Value $CurrentStep

    Pop-Location
    
    Stop-Computer    
}

$CurrentStep++

# Cleanup temporary registry key
Push-Location
Set-Location HKCU:

$isRegistryKeyPresent = Test-Path .\Software\sysprepvagrantserver2012

if ($isRegistryKeyPresent -eq $True)
{
    Remove-Item -Path .\Software\sysprepvagrantserver2012 -Recurse
}

Pop-Location