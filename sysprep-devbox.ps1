# TODO Add attribution

# Boxstarter options
$Boxstarter.Log="$env:temp\boxstarter.log"
$Boxstarter.RebootOk=$true # Allow reboots?
$Boxstarter.NoPassword=$false # Is this a machine with no login password?
$Boxstarter.AutoLogin=$true # Save my password securely and auto-login after a reboot

# Store script progress for use throughout the script
$CurrentStep = 0

# Add a reg key to cheat a little and store our progress, making this script safely repeatable is beyond my powershell abilities currently
Push-Location
Set-Location HKCU:

$isRegistryKeyPresent = Test-Path .\Software\sysprepdevbox

if ($isRegistryKeyPresent -eq $False)
{
    New-Item -Path .\Software -Name sysprepdevbox
    New-ItemProperty ".\Software\sysprepdevbox" -Name "currentStep" -Value $CurrentStep -PropertyType "DWord"
}

$CurrentStep++
$value = (Get-ItemProperty -Path ".\Software\sysprepdevbox" -Name currentStep).currentStep

Pop-Location

if ($CurrentStep -gt $value)
{
    # Configure Administrator user
    Write-Host "Configuring Administrator user..."

    # Prompt user for passoword (stored securely)
    $pass = Read-Host 'Admin user password:' -AsSecureString

    $admin=[adsi]"WinNT://./Administrator,user"
    $admin.psbase.rename("TEHADMIN")
    $admin.SetPassword([Runtime.InteropServices.Marshal]::PtrToStringAuto([Runtime.InteropServices.Marshal]::SecureStringToBSTR($pass)))
    
    # Set password does not expire flag
    $admin.UserFlags.value = $admin.UserFlags.value -bor 0x10000
    $admin.CommitChanges()

    Push-Location
    Set-Location HKCU:

    Set-ItemProperty -Path .\Software\sysprepdevbox -Name currentStep -Value $CurrentStep

    Pop-Location

    Invoke-Reboot
}

$CurrentStep++

if ($CurrentStep -gt $value)
{
    Write-Host "Please set the cleanup options to use later in the installation process."
    C:\Windows\System32\cleanmgr.exe /d c: /sageset:1

    # Inform the user that this script is now in unattended mode
    Write-Host "Configuration complete - script can now be left unattended."

    Push-Location
    Set-Location HKCU:

    Set-ItemProperty -Path .\Software\sysprepdevbox -Name currentStep -Value $CurrentStep

    Pop-Location
}

$CurrentStep++

if ($CurrentStep -gt $value)
{
    # Shrink PageFile
    Write-Host "Configuring pagefile..."

    $System = GWMI Win32_ComputerSystem -EnableAllPrivileges
    $System.AutomaticManagedPagefile = $False
    $System.Put()

    $CurrentPageFile = gwmi -query "select * from Win32_PageFileSetting where name='c:\\pagefile.sys'"
    $CurrentPageFile.InitialSize = 512
    $CurrentPageFile.MaximumSize = 512
    $CurrentPageFile.Put()

    Push-Location
    Set-Location HKCU:

    Set-ItemProperty -Path .\Software\sysprepdevbox -Name currentStep -Value $CurrentStep

    Pop-Location
}

$CurrentStep++

if ($CurrentStep -gt $value)
{
    # Powershell execution policy
    Write-Host "Configuring powershell execution policy..."

    Update-ExecutionPolicy RemoteSigned

    Push-Location
    Set-Location HKCU:

    Set-ItemProperty -Path .\Software\sysprepdevbox -Name currentStep -Value $CurrentStep

    Pop-Location
}

$CurrentStep++

if ($CurrentStep -gt $value)
{
    # Update Windows and reboot if necessary
    Write-Host "Installing updates..."

    Enable-MicrosoftUpdate
    Install-WindowsUpdate -AcceptEula

    Push-Location
    Set-Location HKCU:

    Set-ItemProperty -Path .\Software\sysprepdevbox -Name currentStep -Value $CurrentStep

    Pop-Location

    if (Test-PendingReboot) { Invoke-Reboot }
}

$CurrentStep++

if ($CurrentStep -gt $value)
{
    # Remove windows annoyances
    Set-WindowsExplorerOptions -EnableShowHiddenFilesFoldersDrives -EnableShowProtectedOSFiles -EnableShowFileExtensions -EnableShowFullPathInTitleBar
    Set-TaskbarOptions -Size Small -Lock -Dock Bottom

    Push-Location
    Set-Location HKCU:

    Set-ItemProperty -Path .\Software\sysprepdevbox -Name currentStep -Value $CurrentStep

    Pop-Location
}

$CurrentStep++

if ($CurrentStep -gt $value)
{
    # Install main package list
    Write-Host "Installing packages..."

    choco install 7zip.install
    choco install autohotkey_l.install
    choco install baregrep
    choco install baretail
    choco install ccleaner
    choco install conemu
    choco install curl
    choco install cygwin
    choco install dependencywalker
    choco install dotnet4.5
    choco install everything
    choco install executor
    choco install foxitreader
    choco install git.install
    choco install googlechrome
    choco install greenshot
    choco install growl
    choco install hg
    choco install ilspy
    choco install kdiff3
    choco install lastpass
    choco install linqpad4.install
    choco install nunit
    choco install paint.net
    choco install poshgit
    choco install posh-hg
    choco install putty
    choco install ransack
    choco install rdcman
    choco install resharper
    choco install sourcetree
    choco install stylecop
    choco install sublimetext3
    choco install sysinternals
    choco install virtualbox
    choco install virtualclonedrive
    choco install windbg
    choco install winmerge
    choco install wireshark
    choco install wget

    Push-Location
    Set-Location HKCU:

    Set-ItemProperty -Path .\Software\sysprepdevbox -Name currentStep -Value $CurrentStep

    Pop-Location
}

$CurrentStep++

if ($CurrentStep -gt $value)
{
    # Cleanup WinSXS update debris
    Write-Host "Cleaning update debris..."

    Dism.exe /online /Cleanup-Image /StartComponentCleanup /ResetBase

    Push-Location
    Set-Location HKCU:

    Set-ItemProperty -Path .\Software\sysprepdevbox -Name currentStep -Value $CurrentStep

    Pop-Location
}

$CurrentStep++

if ($CurrentStep -gt $value)
{
    # Additional disk cleanup - execution step
    Write-Host "Cleaning disk..."

    C:\Windows\System32\cleanmgr.exe /d c: /sagerun:1

    Push-Location
    Set-Location HKCU:

    Set-ItemProperty -Path .\Software\sysprepdevbox -Name currentStep -Value $CurrentStep

    Pop-Location
}

$CurrentStep++

if ($CurrentStep -gt $value)
{
    # Defragment the drive
    Write-Host "Defragmenting drive..."

    Optimize-Volume -DriveLetter C

    Push-Location
    Set-Location HKCU:

    Set-ItemProperty -Path .\Software\sysprepdevbox -Name currentStep -Value $CurrentStep

    Pop-Location
}

$CurrentStep++

if ($CurrentStep -gt $value)
{
    # Purge any hidden data
    choco install wget
    
    wget http://download.sysinternals.com/files/SDelete.zip -OutFile sdelete.zip
    
    Add-Type -Assembly System.IO.Compression.FileSystem
    [System.IO.Compression.ZipFile]::ExtractToDirectory("sdelete.zip", ".") 
    ./sdelete.exe -z c:

    Push-Location
    Set-Location HKCU:

    Set-ItemProperty -Path .\Software\sysprepdevbox -Name currentStep -Value $CurrentStep

    Pop-Location
}

$CurrentStep++

if ($CurrentStep -gt $value)
{
    # Shutdown - Script will run on next startup, but only to clean up after itself.
    Write-Host "Shutting down machine..."
    
    Push-Location
    Set-Location HKCU:

    Set-ItemProperty -Path .\Software\sysprepdevbox -Name currentStep -Value $CurrentStep

    Pop-Location
    
    Stop-Computer    
}

$CurrentStep++

# Cleanup temporary registry key
Push-Location
Set-Location HKCU:

$isRegistryKeyPresent = Test-Path .\Software\sysprepdevbox

if ($isRegistryKeyPresent -eq $True)
{
    Remove-Item -Path .\Software\sysprepdevbox -Recurse
}

Pop-Location