# Boxstarter options
$Boxstarter.Log="$env:temp\boxstarter.log"
$Boxstarter.RebootOk=$true # Allow reboots?
$Boxstarter.NoPassword=$false # Is this a machine with no login password?
$Boxstarter.AutoLogin=$true # Save my password securely and auto-login after a reboot

# Powershell execution policy
Update-ExecutionPolicy Unrestricted

# Basic setup
Set-ExplorerOptions -showHidenFilesFoldersDrives -showProtectedOSFiles -showFileExtensions
Set-WindowsExplorerOptions -EnableShowHiddenFilesFoldersDrives -DisableShowProtectedOSFiles -EnableShowFileExtensions -EnableShowFullPathInTitleBar
Set-TaskbarOptions -Size Small -Lock -Dock Bottom
Enable-RemoteDesktop
Disable-InternetExplorerESC
Enable-UAC
if (Test-PendingReboot) { Invoke-Reboot }

# Update Windows and reboot if necessary
Enable-MicrosoftUpdate
Install-WindowsUpdate -AcceptEula
if (Test-PendingReboot) { Invoke-Reboot }
Dism.exe /online /Cleanup-Image /StartComponentCleanup /ResetBase # Cleanup WinSXS update debris

# Install chocolatey
cinstm chocolatey

# Install Visual Studio 2013 Professional 
cinstm VisualStudio2013Professional -InstallArguments WebTools # Work out correct args here
if (Test-PendingReboot) { Invoke-Reboot }

cinstm DotNet3.5 # Not automatically installed with VS 2013. Includes .NET 2.0. Uses Windows Features to install.
if (Test-PendingReboot) { Invoke-Reboot }

#Browsers
cinstm googlechrome
cinstm firefox

#Other essential tools
cinstm 7zip.install
cinstm adobereader
cinstm javaruntime
cinstm TeraCopy

cinstm notepadplusplus.install
cinstm vlc
cinstm git.install
cinstm flashplayerplugin
cinstm ccleaner
cinstm ruby
cinstm sysinternals
cinstm python
cinstm filezilla
cinstm dropbox
cinstm DotNet4.5
cinstm paint.net
cinstm virtualbox
cinstm vim
cinstm linqpad4
cinstm NugetPackageExplorer
cinstm windbg

Install-ChocolateyPinnedTaskBarItem "$($Boxstarter.programFiles86)\Google\Chrome\Application\chrome.exe"
Install-ChocolateyPinnedTaskBarItem "$($Boxstarter.programFiles86)\Microsoft Visual Studio 12.0\Common7\IDE\devenv.exe"